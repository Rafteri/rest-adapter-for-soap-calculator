package com.dinrafikov.calc.adapter.dto.request;

import com.dinrafikov.calc.adapter.validation.IntegerNotEquals;
import com.dinrafikov.calc.adapter.validation.markers.CalcDivision;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 14.03.2021
 *
 * @author Dinar Rafikov
 */
@Data
public class CalcRequestDto {

    @ApiModelProperty(required = true)
    @NotNull
    private Integer a;

    @ApiModelProperty(required = true)
    @NotNull
    @IntegerNotEquals(invalidValues = 0, groups = CalcDivision.class, message = "Can't be zero")
    private Integer b;
}
