package com.dinrafikov.calc.adapter.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

/**
 * 14.03.2021
 *
 * @author Dinar Rafikov
 */
@Data
@AllArgsConstructor
public class SoapCalcRequestDto {
    private int a;
    private int b;
    private Operation operation;

    public String getUniqueKey() {
        if (operation.isCommutative()) {
            return operation.name() + "&" + Math.min(a, b) + "&" + Math.max(a, b);
        } else {
            return operation.name() + "&" + a + "&" + b;
        }
    }

    @Getter
    @AllArgsConstructor
    public enum Operation {
        ADD(true),
        SUBTRACT(false),
        DIVIDE(false),
        MULTIPLY(true);

        private final boolean isCommutative;
    }
}
