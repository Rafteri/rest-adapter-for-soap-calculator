package com.dinrafikov.calc.adapter.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 14.03.2021
 *
 * @author Dinar Rafikov
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CalcResultDto {
    private Integer result;
}
