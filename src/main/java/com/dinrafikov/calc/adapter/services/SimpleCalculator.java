package com.dinrafikov.calc.adapter.services;

import com.dinrafikov.calc.adapter.dto.request.CalcRequestDto;
import com.dinrafikov.calc.adapter.dto.response.CalcResultDto;

/**
 * 14.03.2021
 *
 * @author Dinar Rafikov
 */
public interface SimpleCalculator {
    CalcResultDto add(CalcRequestDto request);

    CalcResultDto subtract(CalcRequestDto request);

    CalcResultDto multiply(CalcRequestDto request);

    CalcResultDto divide(CalcRequestDto request);
}
