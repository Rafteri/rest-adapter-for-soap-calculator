package com.dinrafikov.calc.adapter.services;

import com.dinrafikov.calc.adapter.dto.request.CalcRequestDto;
import com.dinrafikov.calc.adapter.dto.request.SoapCalcRequestDto;
import com.dinrafikov.calc.adapter.dto.response.CalcResultDto;
import lombok.RequiredArgsConstructor;
import org.apache.camel.Produce;
import org.springframework.stereotype.Service;

import java.util.function.Function;

import static com.dinrafikov.calc.adapter.dto.request.SoapCalcRequestDto.Operation;

/**
 * 14.03.2021
 *
 * @author Dinar Rafikov
 */
@Service
@RequiredArgsConstructor
public class SoapCalcAdapter implements SimpleCalculator {

    @Produce("direct:soap-calc")
    private Function<SoapCalcRequestDto, CalcResultDto> producer;

    @Override
    public CalcResultDto add(final CalcRequestDto request) {
        return producer.apply(new SoapCalcRequestDto(request.getA(), request.getB(), Operation.ADD));
    }

    @Override
    public CalcResultDto subtract(final CalcRequestDto request) {
        return producer.apply(new SoapCalcRequestDto(request.getA(), request.getB(), Operation.SUBTRACT));
    }

    @Override
    public CalcResultDto multiply(final CalcRequestDto request) {
        return producer.apply(new SoapCalcRequestDto(request.getA(), request.getB(), Operation.MULTIPLY));
    }

    @Override
    public CalcResultDto divide(final CalcRequestDto request) {
        return producer.apply(new SoapCalcRequestDto(request.getA(), request.getB(), Operation.DIVIDE));
    }
}
