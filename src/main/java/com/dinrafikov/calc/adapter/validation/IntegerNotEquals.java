package com.dinrafikov.calc.adapter.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

/**
 * 14.03.2021
 *
 * @author Dinar Rafikov
 */
@Documented
@Constraint(validatedBy = NotEqualsValidator.class)
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface IntegerNotEquals {
    String message() default "Shouldn't be equal";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    int[] invalidValues();
}
