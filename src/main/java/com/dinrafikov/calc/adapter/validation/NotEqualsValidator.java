package com.dinrafikov.calc.adapter.validation;

import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 14.03.2021
 *
 * @author Dinar Rafikov
 */
@Component
public class NotEqualsValidator implements ConstraintValidator<IntegerNotEquals, Integer> {

    private Set<Integer> invalidValues;

    @Override
    public void initialize(final IntegerNotEquals constraintAnnotation) {
        invalidValues = IntStream.of(constraintAnnotation.invalidValues())
                .boxed()
                .collect(Collectors.toSet());
    }

    @Override
    public boolean isValid(final Integer value, final ConstraintValidatorContext context) {
        return value == null || !invalidValues.contains(value);
    }
}
