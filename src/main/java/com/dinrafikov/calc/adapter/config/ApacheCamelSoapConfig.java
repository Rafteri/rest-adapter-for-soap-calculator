package com.dinrafikov.calc.adapter.config;

import org.apache.camel.dataformat.soap.name.ServiceInterfaceStrategy;
import org.apache.camel.model.dataformat.SoapJaxbDataFormat;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tempuri.CalculatorSoap;

/**
 * 14.03.2021
 *
 * @author Dinar Rafikov
 */
@Configuration
public class ApacheCamelSoapConfig {

    @Bean
    public SoapJaxbDataFormat calcSoapJaxb() {
        final SoapJaxbDataFormat soap = new SoapJaxbDataFormat(
                "org.tempuri",
                new ServiceInterfaceStrategy(CalculatorSoap.class, true)
        );
        soap.setVersion("1.2");
        return soap;
    }
}
