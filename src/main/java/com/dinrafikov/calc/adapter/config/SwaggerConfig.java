package com.dinrafikov.calc.adapter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * 14.03.2021
 *
 * @author Dinar Rafikov
 */
@Configuration
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.dinrafikov.calc.adapter.controllers"))
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(
                        new ApiInfoBuilder()
                                .title("REST Calculator API")
                                .version("v1")
                                .build()
                );
    }
}