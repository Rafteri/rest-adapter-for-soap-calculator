package com.dinrafikov.calc.adapter.routes;

import com.dinrafikov.calc.adapter.dto.request.SoapCalcRequestDto;
import com.dinrafikov.calc.adapter.dto.response.CalcResultDto;
import org.apache.camel.ExchangePattern;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.infinispan.InfinispanConstants;
import org.apache.camel.component.infinispan.InfinispanOperation;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.tempuri.*;

import javax.xml.ws.soap.SOAPFaultException;

/**
 * 14.03.2021
 *
 * @author Dinar Rafikov
 */
@Component
public class SoapCalcRoute extends RouteBuilder {

    private static final String SOAP_CALC_REQUEST_PROPERTY = "soapCalcRequest";
    private static final String SOAP_CALC_REQUEST_CACHE_PROPERTY = "soapCalcRequestCacheKey";
    private static final String SOAP_CALC_RESPONSE_PROPERTY = "soapCalcResponse";

    @Override
    public void configure() throws Exception {
        onException(SOAPFaultException.class)
                .throwException(new HttpServerErrorException(HttpStatus.BAD_GATEWAY))
                .handled(true);

        from("direct:soap-calc")
                .process(setUpProperties)

                .setHeader(InfinispanConstants.OPERATION).constant(InfinispanOperation.GET)
                .setHeader(InfinispanConstants.KEY).exchangeProperty(SOAP_CALC_REQUEST_CACHE_PROPERTY)
                .to("infinispan://default")

                .choice()
                .when((exchange -> exchange.getMessage().getBody() != null))
                .stop()
                .otherwise()

                .process(restToSoapProcessor)
                .marshal("calcSoapJaxb")
                .setHeader(HttpHeaders.CONTENT_TYPE, () -> "application/soap+xml")
                .to("cxf://http://www.dneonline.com/calculator.asmx?dataFormat=RAW")
                .unmarshal("calcSoapJaxb")
                .process(soapToDtoProcessor)

                .setHeader(InfinispanConstants.OPERATION).constant(InfinispanOperation.PUT)
                .setHeader(InfinispanConstants.KEY).exchangeProperty(SOAP_CALC_REQUEST_CACHE_PROPERTY)
                .setHeader(InfinispanConstants.VALUE).exchangeProperty(SOAP_CALC_RESPONSE_PROPERTY)
                .to("infinispan://default")

                .setBody(exchangeProperty(SOAP_CALC_RESPONSE_PROPERTY))
        ;
    }

    private final Processor setUpProperties = exchange -> {
        final SoapCalcRequestDto requestDto = exchange.getIn().getBody(SoapCalcRequestDto.class);
        exchange.setProperty(SOAP_CALC_REQUEST_PROPERTY, requestDto);
        exchange.setProperty(SOAP_CALC_REQUEST_CACHE_PROPERTY, requestDto.getUniqueKey());
    };

    private final Processor restToSoapProcessor = exchange -> {
        final Message message = exchange.getIn();
        final SoapCalcRequestDto requestDto = exchange.getProperty(SOAP_CALC_REQUEST_PROPERTY, SoapCalcRequestDto.class);
        switch (requestDto.getOperation()) {
            case ADD:
                final Add addition = new Add();
                addition.setIntA(requestDto.getA());
                addition.setIntB(requestDto.getB());
                message.setBody(addition);
                break;
            case DIVIDE:
                final Divide divide = new Divide();
                divide.setIntA(requestDto.getA());
                divide.setIntB(requestDto.getB());
                message.setBody(divide);
                break;
            case MULTIPLY:
                final Multiply multiply = new Multiply();
                multiply.setIntA(requestDto.getA());
                multiply.setIntB(requestDto.getB());
                message.setBody(multiply);
                break;
            case SUBTRACT:
                final Subtract subtract = new Subtract();
                subtract.setIntA(requestDto.getA());
                subtract.setIntB(requestDto.getB());
                message.setBody(subtract);
                break;
            default:
                throw new IllegalStateException("Unknown soap calc operation " + requestDto.getOperation());
        }
    };

    private final Processor soapToDtoProcessor = exchange -> {
        final Message exchangeMessage = exchange.getMessage();
        final SoapCalcRequestDto requestDto = exchange.getProperty(SOAP_CALC_REQUEST_PROPERTY, SoapCalcRequestDto.class);
        final CalcResultDto resultDto = new CalcResultDto();
        final Message message = exchange.getIn();
        switch (requestDto.getOperation()) {
            case ADD:
                final AddResponse addResponse = exchangeMessage.getBody(AddResponse.class);
                resultDto.setResult(addResponse.getAddResult());
                break;
            case SUBTRACT:
                final SubtractResponse subtractResponse = exchangeMessage.getBody(SubtractResponse.class);
                resultDto.setResult(subtractResponse.getSubtractResult());
                break;
            case MULTIPLY:
                final MultiplyResponse multiplyResponse = exchangeMessage.getBody(MultiplyResponse.class);
                resultDto.setResult(multiplyResponse.getMultiplyResult());
                break;
            case DIVIDE:
                final DivideResponse divideResponse = exchangeMessage.getBody(DivideResponse.class);
                resultDto.setResult(divideResponse.getDivideResult());
                break;
            default:
                throw new IllegalStateException("Unknown soap calc operation " + requestDto.getOperation());
        }
        message.getHeaders().clear();
        message.setBody(resultDto);
        exchange.setProperty(SOAP_CALC_RESPONSE_PROPERTY, resultDto);
    };
}
