package com.dinrafikov.calc.adapter.controllers;

import com.dinrafikov.calc.adapter.dto.request.CalcRequestDto;
import com.dinrafikov.calc.adapter.dto.response.CalcResultDto;
import com.dinrafikov.calc.adapter.services.SimpleCalculator;
import com.dinrafikov.calc.adapter.validation.markers.CalcDivision;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.groups.Default;

/**
 * 14.03.2021
 *
 * @author Dinar Rafikov
 */
@Slf4j
@Api(tags = "REST calculator")
@RestController
@RequestMapping("/calculator")
@RequiredArgsConstructor
public class CalculatorController {

    private final SimpleCalculator calculator;

    @ApiOperation("Get sum of two integers numbers")
    @GetMapping("/addition")
    public CalcResultDto add(@Validated final CalcRequestDto requestDto) {
        return calculator.add(requestDto);
    }

    @ApiOperation("Get subtraction of two integers numbers")
    @GetMapping("/subtraction")
    public CalcResultDto subtract(@Validated final CalcRequestDto requestDto) {
        return calculator.subtract(requestDto);
    }

    @ApiOperation("Get multiplication of two integers numbers")
    @GetMapping("/multiplication")
    public CalcResultDto multiply(@Validated final CalcRequestDto requestDto) {
        return calculator.multiply(requestDto);
    }

    @ApiOperation("Get division of two integers numbers")
    @GetMapping("/division")
    public CalcResultDto divide(@Validated(value = {Default.class, CalcDivision.class}) final CalcRequestDto requestDto) {
        return calculator.divide(requestDto);
    }
}
